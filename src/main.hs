import Application (makeApplication)
import Settings (parseExtra)
import Yesod.Default.Config (fromArgs)
import Yesod.Default.Main (defaultMainLog)
import Prelude (IO)

main :: IO ()
main = defaultMainLog (fromArgs parseExtra) makeApplication
