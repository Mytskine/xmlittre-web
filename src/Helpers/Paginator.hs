module Helpers.Paginator (
    widget,
    getCurrentPage,
    getFirstItemRank,
) where

import Import
import qualified Yesod.Paginator.Pages as Pages (toPages)
import qualified Yesod.Paginator.Paginate as Paginate (getCurrentPage)
import Yesod.Paginator.Widgets (simple)

-- Up to this page links shown at the same time
maxPagesLinks :: Num p => p
maxPagesLinks = 20

-- Up to this items par page
maxItemsPerPage :: Num p => p
maxItemsPerPage = 50

widget :: Integral a => a -> WidgetFor m ()
widget totalResults = do
    currentPage <- Paginate.getCurrentPage
    let total = fromInteger $ toInteger totalResults
    let pages = Pages.toPages currentPage maxItemsPerPage total []
    simple maxPagesLinks pages

getCurrentPage :: Handler Int
getCurrentPage = fromInteger . toInteger <$> Paginate.getCurrentPage

getFirstItemRank :: Handler Int
getFirstItemRank = do
    currentPage <- getCurrentPage
    return $ 1 + maxPagesLinks * (currentPage - 1)
